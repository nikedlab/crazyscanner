TEMPLATE += app
QT += widgets core gui
HEADERS += \
    filedialog.h \
    mainwindow.h

SOURCES += \
    main.cpp \
    filedialog.cpp \
    mainwindow.cpp

FORMS += \
    filedialog.ui \
    mainwindow.ui
